<?php

namespace TBureck\Diversity\Library\People;

use TBureck\Diversity\Library\People\Authorization\Permission\GroupPermissionValueInterface;

/**
 * @author Tim Bureck
 * @since 2016-04-01
 */
interface GroupInterface
{

    /**
     * A unique identifier
     *
     * @return mixed
     */
    public function getId();

    /**
     * Get the group's name
     *
     * @return string the group's name
     */
    public function getName();

    /**
     * @param string $name the group's new name
     */
    public function setName($name);

    /**
     * Returns a format string, which defines the look of a user's name when shown in lists
     *
     * @return string the group format
     */
    public function getFormat();

    /**
     * The format will determine how user names are displayed in lists. You can use any HTML code to style the format.
     * The format will be passed to a sprintf() function call, with the user name being the first argument %s.
     *
     * @param string $format the group's new format
     */
    public function setFormat($format);

    /**
     * Sets the permissions of this group. The given Collection should only contain objects of type
     * TBureck\Diversity\Library\People\Authorization\Permission\GroupPermissionValueInterface. All other objects should be
     * stripped out.
     *
     * @param GroupPermissionValueInterface[] $permissions the new set of permissions to be set
     */
    public function setPermissions($permissions);

    /**
     * Assigns a new value to the given permission.
     *
     * @param GroupPermissionValueInterface $permission
     */
    public function setPermission(GroupPermissionValueInterface $permission);

    /**
     * Gets a permission value object for a specific permission.
     *
     * @param string $name the name of the permission
     * 
     * @return GroupPermissionValueInterface
     */
    public function getPermission($name);

    /**
     * Get the permission assigned to this group
     *
     * @return GroupPermissionValueInterface[]
     */
    public function getPermissions();

}
