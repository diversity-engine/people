# Diversity Engine People Library

**diversity/people** is a library that provides basic functionality for handling users and permissions. Users can be a member of one or more groups. Groups have a set of permissions that determine whether or not the group can perform actions within the application. Single permissions can be overridden on a per-user basis.

Test
