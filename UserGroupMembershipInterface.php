<?php

namespace TBureck\Diversity\Library\People;

/**
 * @author Tim Bureck
 * @since 2016-04-01
 */
interface UserGroupMembershipInterface
{

    /**
     * @return GroupInterface the group this user is assigned to
     */
    public function getGroup();

    /**
     * @return \DateTime the date and time when the user has joined this group
     */
    public function getJoinTime();

    /**
     * @return UserInterface the affected user
     */
    public function getUser();

}
