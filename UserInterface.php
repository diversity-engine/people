<?php

namespace TBureck\Diversity\Library\People;

use TBureck\Diversity\Library\People\Authorization\Permission\PermissionValueInterface;
use TBureck\Diversity\Library\People\Authorization\Permission\UserPermissionValueInterface;

/**
 * Interface UserInterface
 * @package TBureck\Diversity\Library\People
 *
 * @author Tim Bureck
 * @since 2015-11-27
 */
interface UserInterface
{

    /**
     * @return mixed the user's unique id
     */
    public function getId();

    /**
     * @return string the user's name
     */
    public function getName();

    /**
     * @return string the user's email address
     */
    public function getEmail();

    /**
     * @return \DateTime the user's creation date and time
     */
    public function getCreatedAt();

    /**
     * Get the value object of a specific permission
     *
     * @param string $name The name of the permission
     * @return PermissionValueInterface
     */
    public function getPermission($name);

    /**
     * Get the permissions assigned to this user
     *
     * @return PermissionValueInterface[]
     */
    public function getPermissions();

    /**
     * Get the permissions that have been overridden for this user.
     *
     * @return PermissionValueInterface[]
     */
    public function getOverriddenPermissions();

    /**
     * @param $permission UserPermissionValueInterface the permission value to be set
     */
    public function setPermission(UserPermissionValueInterface $permission);

    /**
     * @param $permission UserPermissionValueInterface the key of the permission to be unset
     */
    public function unsetPermission(UserPermissionValueInterface $permission);

    /**
     * Sets the permissions of this user. The given \Traversable should only contain objects of type
     * UserPermissionValueInterface. All other objects should be stripped out.
     *
     * @param \Traversable|UserPermissionValueInterface[] $permissions
     */
    public function setPermissions(\Traversable $permissions);

    /**
     * @param $name string the permission key to check
     * @return bool true, if the given permission is overridden for this particular user
     */
    public function isPermissionOverridden($name);
}