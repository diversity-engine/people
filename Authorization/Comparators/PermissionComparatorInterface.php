<?php

namespace TBureck\Diversity\Library\People\Authorization\Comparators;

/**
 * @author Tim Bureck
 * @since 2016-04-01
 */
interface PermissionComparatorInterface
{

    /**
     * This method will compare two permission values.
     *
     * @param mixed $a
     * @param mixed $b
     * @return boolean true, if $a is dominant over $b or $a is equal to $b. false otherwise
     */
    public function compare($a, $b);

}
