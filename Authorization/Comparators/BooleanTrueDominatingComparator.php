<?php

namespace TBureck\Diversity\Library\People\Authorization\Comparators;

/**
 * This comparator is for yes/no permissions, which are true dominant. That means, that the permission will be true
 * for a user, whenever the user is in a group that has got a true value in the given permission.
 *
 * @author Tim Bureck
 * @since 2016-04-01
 */
class BooleanTrueDominatingComparator implements PermissionComparatorInterface
{

    /**
     * This method will compare two permission values.
     *
     * @param mixed $a
     * @param mixed $b
     * @return boolean true, if $a is dominant over $b or $a is equal to $b. false otherwise
     */
    public function compare($a, $b)
    {
        $aVal = (boolean)$a;
        $bVal = (boolean)$b;

        if ($bVal && !$aVal) {
            return false;
        }

        return true;
    }

}
