<?php

namespace TBureck\Diversity\Library\People\Authorization\Permission;

use TBureck\Diversity\Library\People\Authorization\Comparators\PermissionComparatorInterface;
use TBureck\Diversity\Library\People\UserGroupMembershipInterface;

/**
 * Class GroupPermissionResolver
 * @package TBureck\Diversity\Master\CoreBundle\Authorization
 *
 * @author Tim Bureck
 * @since 2016-04-01
 */
class DefaultGroupPermissionResolver implements GroupPermissionResolverInterface
{

    /**
     * @inheritdoc
     */
    public function resolve(array $groups)
    {
        $permissions = [];

        /** @var $g UserGroupMembershipInterface */
        foreach ($groups as $g) {
            if (!($g instanceof UserGroupMembershipInterface)) {
                continue;
            }

            /** @var $perm GroupPermissionValueInterface */
            foreach ($g->getGroup()->getPermissions() as $perm) {
                if (!($perm instanceof GroupPermissionValueInterface)) {
                    continue;
                }

                $title = $perm->getPermission()->getName();

                // Check if permission has already been added. If not, add it and continue.
                if (!array_key_exists($title, $permissions)) {
                    $permissions[$title] = $perm;
                } else {
                    // Permission has been added. Check whether this permission is dominant over already saved one
                    /** @var $comparator PermissionComparatorInterface */
                    $clazz = $perm->getPermission()->getClass();
                    $comparator = new $clazz();

                    if ($comparator->compare($perm, $permissions[$title]) == $perm) {
                        // Permission is dominant over already saved one, overwrite.
                        $permissions[$title] = $perm;
                    }
                }
            }
        }

        return $permissions;
    }

}
