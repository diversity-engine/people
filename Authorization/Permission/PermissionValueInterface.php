<?php

namespace TBureck\Diversity\Library\People\Authorization\Permission;

/**
 * @author Tim Bureck
 * @since 2016-04-01
 */
interface PermissionValueInterface
{

    /**
     * Returns the permission, which the value is assigned to
     *
     * @return GroupPermissionInterface
     */
    public function getPermission();

    /**
     * @param GroupPermissionInterface $permission
     */
    public function setPermission(GroupPermissionInterface $permission);

    /**
     * The value
     *
     * @return mixed
     */
    public function getValue();

    /**
     * @param mixed $value the value
     */
    public function setValue($value);

}
