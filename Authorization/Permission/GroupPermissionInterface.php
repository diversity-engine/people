<?php

namespace TBureck\Diversity\Library\People\Authorization\Permission;

/**
 * @author Tim Bureck
 * @since 2016-04-01
 */
interface GroupPermissionInterface
{

    /**
     * Unique identifier
     *
     * @return mixed
     */
    public function getId();

    /**
     * the name of the group permission being used internally
     *
     * @return string the group permission name
     */
    public function getName();

    /**
     * Title of the group permission being shown to the user
     *
     * @return string
     */
    public function getTitle();

    /**
     * A short description of this group permission
     *
     * @return string
     */
    public function getDescription();

    /**
     * Fully qualified identifier of the group permission comparator interface, that should compare the values of
     * this permission.
     *
     * @return string
     */
    public function getClass();

    /**
     * Returns the default value of this group permission, which will be assigned to newly generated groups by default.
     *
     * @return mixed the default value of this permission
     */
    public function getDefaultValue();

    // TODO how to map allowed values in DB?
    /**
     * @return string[]
     */
//	public function getAllowedValues();

    /**
     * Should return the form type, which allows the user to edit the values managed by this comparator.
     *
     * @return string
     */
    public function getFormType();

}
