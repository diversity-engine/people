<?php

namespace TBureck\Diversity\Library\People\Authorization\Permission;

/**
 * Class InMemoryGroupPermissionHolder
 * @package TBureck\Diversity\Library\People\Authorization
 *
 * @author Tim Bureck
 * @since 2016-04-01
 */
class InMemoryGroupPermissionHolder implements GroupPermissionHolderInterface
{

    /**
     * @var array|GroupPermissionInterface[]
     */
    protected $permissions;

    /**
     * InMemoryGroupPermissionHolder constructor.
     *
     * @param array $permissions
     */
    public function __construct(array $permissions)
    {
        // TODO add logging in order to warn about non GroupPermissionInterface objects?
        foreach ($permissions as $permission) {
            $this->set($permission);
        }
    }

    /**
     * @inheritDoc
     */
    public function set(GroupPermissionInterface $permission)
    {
        $this->permissions[$permission->getName()] = $permission;
    }

    /**
     * @inheritdoc
     */
    public function get($permissionName)
    {
        return
            array_key_exists($permissionName, $this->permissions)
                ? $this->permissions[$permissionName]
                : null;
    }

    /**
     * @inheritdoc
     */
    public function getPermissions()
    {
        return $this->permissions;
    }
}