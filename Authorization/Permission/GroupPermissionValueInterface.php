<?php

namespace TBureck\Diversity\Library\People\Authorization\Permission;

use TBureck\Diversity\Library\People\GroupInterface;

/**
 * @author Tim Bureck
 * @since 2016-04-01
 */
interface GroupPermissionValueInterface extends PermissionValueInterface
{

    /**
     * Returns the group, which this permission value is assigned to
     *
     * @return \TBureck\Diversity\Library\People\GroupInterface
     */
    public function getGroup();

}
