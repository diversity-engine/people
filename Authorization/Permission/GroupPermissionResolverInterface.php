<?php

namespace TBureck\Diversity\Library\People\Authorization\Permission;

use TBureck\Diversity\Library\People\UserGroupMembershipInterface;

/**
 * Class GroupPermissionResolver
 * @package TBureck\Diversity\Master\CoreBundle\Authorization
 *
 * @author Tim Bureck
 * @since 2016-04-01
 */
interface GroupPermissionResolverInterface
{

    /**
     * This method takes a set of group memberships and resolves their respective permissions to a single set of
     * permissions, taking into account which permission settings are superior to others.
     *
     * @param UserGroupMembershipInterface|array $groups the groups to resolve
     *
     * @return PermissionValueInterface[]|array list of permissions to use using the given set of group memberships
     */
    public function resolve(array $groups);
}