<?php

namespace TBureck\Diversity\Library\People\Authorization\Permission;

/**
 * Interface GroupPermissionHolderInterface
 * @package TBureck\Diversity\Library\People\Authorization
 *
 * @author Tim Bureck
 * @since 2016-04-01
 */
interface GroupPermissionHolderInterface
{

    /**
     * Adds the given permission to this holder.
     *
     * @param GroupPermissionInterface $permission
     */
    public function set(GroupPermissionInterface $permission);

    /**
     * Gets the permission with the given name or null, if it is not known to this holder.
     *
     * @param string $permissionName
     *
     * @return GroupPermissionInterface|null
     */
    public function get($permissionName);

    /**
     * @return GroupPermissionInterface[] an array of all permissions known to this holder.
     */
    public function getPermissions();

} 