<?php

namespace TBureck\Diversity\Library\People\Authorization\Permission;

use TBureck\Diversity\Library\People\UserInterface;

/**
 * @author Tim Bureck
 * @since 2016-04-01
 */
interface UserPermissionValueInterface extends PermissionValueInterface
{

    /**
     * Returns the user, which this permission value is assigned to
     *
     * @return UserInterface
     */
    public function getUser();

}
